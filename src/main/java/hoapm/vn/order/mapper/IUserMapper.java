package hoapm.vn.order.mapper;

import hoapm.vn.order.dto.UsersDTO;
import hoapm.vn.order.entity.UserEntity;

public interface IUserMapper extends IGenericMapper<UsersDTO, UserEntity> {
}
