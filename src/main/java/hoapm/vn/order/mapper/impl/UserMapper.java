package hoapm.vn.order.mapper.impl;

import hoapm.vn.order.dto.UsersDTO;
import hoapm.vn.order.entity.UserEntity;
import hoapm.vn.order.mapper.IUserMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class UserMapper implements IUserMapper {
    @Override
    public UsersDTO mapToDTO(UserEntity userEntity) {
        return UsersDTO.builder()
                .id(userEntity.getId())
                .name(userEntity.getName())
                .phone(userEntity.getPhone())
                .build();
    }

    @Override
    public UserEntity mapToEntity(UsersDTO usersDTO) {
        return UserEntity
                .builder()
                .id(usersDTO.getId())
                .name(usersDTO.getName())
                .phone(usersDTO.getPhone())
                .build();
    }

    @Override
    public List<UsersDTO> mapToListDTO(List<UserEntity> users) {
        if (users.size() > 0) {
            return users.stream().map(this::mapToDTO)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public List<UserEntity> mapToListEntity(List<UsersDTO> usersDTOS) {
        if (usersDTOS.size() > 0 ){
            return usersDTOS.stream().map(this::mapToEntity)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
