package hoapm.vn.order.mapper.impl;

import hoapm.vn.order.dto.OrdersDTO;
import hoapm.vn.order.entity.OrderEntity;
import hoapm.vn.order.mapper.IOrderMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;
import java.util.stream.Collectors;

@Component
public class OrderMapper implements IOrderMapper {
    @Override
    public OrdersDTO mapToDTO(OrderEntity orderEntity) {
        if (Objects.isNull(orderEntity)) {
            return new OrdersDTO();
        }
        return OrdersDTO.builder()
                .id(orderEntity.getId())
                .userId(orderEntity.getUserId())
                .total(orderEntity.getTotal())
                .number(orderEntity.getNumber())
                .build();
    }

    @Override
    public OrderEntity mapToEntity(OrdersDTO ordersDTO) {
        return OrderEntity.builder()
                .id(ordersDTO.getId())
                .userId(ordersDTO.getUserId())
                .total(ordersDTO.getTotal())
                .number(ordersDTO.getNumber())
                .build();
    }

    @Override
    public List<OrdersDTO> mapToListDTO(List<OrderEntity> orders) {
        if (orders.size() > 0) {
            return orders.stream().map(this::mapToDTO).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public List<OrderEntity> mapToListEntity(List<OrdersDTO> ordersDTOS) {
        if (ordersDTOS.size() > 0 ){
            return ordersDTOS.stream().map(this::mapToEntity).collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
