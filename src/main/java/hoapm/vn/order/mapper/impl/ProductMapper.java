package hoapm.vn.order.mapper.impl;

import hoapm.vn.order.dto.ProducDTO;
import hoapm.vn.order.entity.ProductEntity;
import hoapm.vn.order.mapper.IProductMapper;
import org.springframework.stereotype.Component;

import java.util.ArrayList;
import java.util.List;
import java.util.stream.Collectors;

@Component
public class ProductMapper implements IProductMapper {
    @Override
    public ProducDTO mapToDTO(ProductEntity product) {
        return ProducDTO.builder()
                .id(product.getId())
                .name(product.getName())
                .price(product.getPrice())
                .build();
    }

    @Override
    public ProductEntity mapToEntity(ProducDTO producDTO) {
        return ProductEntity
                .builder()
                .id(producDTO.getId())
                .name(producDTO.getName())
                .price(producDTO.getPrice())
                .build();
    }

    @Override
    public List<ProducDTO> mapToListDTO(List<ProductEntity> products) {
        if (products.size() > 0 ) {
            return products.stream().map(this::mapToDTO)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }

    @Override
    public List<ProductEntity> mapToListEntity(List<ProducDTO> producDTOS) {
        if (producDTOS.size() > 0 ) {
            return producDTOS.stream().map(this::mapToEntity)
                    .collect(Collectors.toList());
        }
        return new ArrayList<>();
    }
}
