package hoapm.vn.order.mapper;

import hoapm.vn.order.dto.ProducDTO;
import hoapm.vn.order.entity.ProductEntity;

public interface IProductMapper extends IGenericMapper <ProducDTO, ProductEntity> {
}
