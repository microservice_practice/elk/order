package hoapm.vn.order.mapper;

import hoapm.vn.order.dto.OrdersDTO;
import hoapm.vn.order.entity.OrderEntity;

public interface IOrderMapper extends IGenericMapper<OrdersDTO, OrderEntity> {
}
