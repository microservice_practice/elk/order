package hoapm.vn.order.config;

import com.fasterxml.jackson.databind.ObjectMapper;
import hoapm.vn.order.dto.ProducDTO;
import hoapm.vn.order.dto.UsersDTO;
import hoapm.vn.order.service.IProductService;
import hoapm.vn.order.service.IUserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.core.io.ClassPathResource;

import java.io.IOException;
import java.util.List;

@Configuration
public class ConfigurationData {

    @Autowired
    private IProductService productService;

    @Autowired
    private IUserService userService;

    private final ObjectMapper objectMapper = new ObjectMapper();


    @Value("${data.init.product.path}")
    private String dataProductPath;

    @Value("${data.init.user.path}")
    private String dataUserPath;

    @Value("${data.init.order.path}")
    private String dataOrderPath;

    private final Logger logger = LoggerFactory.getLogger(ConfigurationData.class.getName());

    @Bean
    public void createProductData() {
        try {
            ClassPathResource classPathResource = new ClassPathResource(dataProductPath);
            Object data = objectMapper.readValue(classPathResource.getInputStream(), objectMapper.getTypeFactory().constructCollectionType(List.class, ProducDTO.class));
            productService.upsertBulk((List<ProducDTO>) data);
        } catch (IOException e) {
            logger.info("Fail to reading data from {} ", dataProductPath);
        }
    }

    @Bean
    public void createUserData() {
        try {
            ClassPathResource classPathResource = new ClassPathResource(dataUserPath);
            Object data = objectMapper.readValue(classPathResource.getInputStream(), objectMapper.getTypeFactory().constructCollectionType(List.class, UsersDTO.class));
            userService.upsertBulk((List<UsersDTO>) data);
        } catch (IOException e) {
            logger.info("Fail to reading data from {} ", dataOrderPath);
        }
    }
}
