package hoapm.vn.order.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Data
@Entity(name = "product")
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class ProductEntity {

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "name")
    private String name;

    @Column(name = "price")
    private Long price;

    @ManyToMany(mappedBy = "products", fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    private List<OrderEntity> orders;

}
