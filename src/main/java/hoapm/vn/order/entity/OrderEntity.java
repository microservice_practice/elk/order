package hoapm.vn.order.entity;

import lombok.*;

import javax.persistence.*;
import java.util.List;

@Entity(name = "orders")
@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrderEntity {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private Long id;

    @Column(name = "total")
    private Long total;

    @Column(name = "number")
    private Long number;

    @ManyToOne(fetch = FetchType.LAZY)
    @JoinColumn(name = "userID", referencedColumnName = "id", nullable = false, insertable = false, updatable = false)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    private UserEntity users;

    @Column(name = "userID", nullable = false)
    private Long userId;

    @ManyToMany(cascade = CascadeType.ALL, fetch = FetchType.LAZY)
    @EqualsAndHashCode.Exclude
    @ToString.Exclude
    @JoinTable(name = "order_detail", joinColumns = @JoinColumn(name = "OrderId"), inverseJoinColumns = @JoinColumn(name = "ProductId"))
    private List<ProductEntity> products;
}
