package hoapm.vn.order.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.ArrayList;
import java.util.List;

@Data
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class OrdersDTO {
    private Long id;
    private Long userId;
    private Long total;
    private Long number;
    private List<Long> productIds = new ArrayList<>();
}
