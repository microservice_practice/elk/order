package hoapm.vn.order.service.impl;

import hoapm.vn.order.dto.ProducDTO;
import hoapm.vn.order.mapper.IProductMapper;
import hoapm.vn.order.repository.IProductRepository;
import hoapm.vn.order.service.IProductService;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.List;

@Service
public class ProductService implements IProductService {
    private final IProductRepository productRepository;

    private final IProductMapper productMapper;


    public ProductService(IProductRepository productRepository, IProductMapper productMapper) {
        this.productRepository = productRepository;
        this.productMapper = productMapper;
    }

    @Transactional
    @Override
    public List<ProducDTO> getAll() {
        return this.productMapper.mapToListDTO(productRepository.findAll());
    }


    @Transactional
    @Override
    public void upsert(ProducDTO product) {
        this.productRepository.save(this.productMapper.mapToEntity(product));
    }

    @Transactional
    @Override
    public void upsertBulk(List<ProducDTO> products) {
        this.productRepository.saveAll(this.productMapper.mapToListEntity(products));
    }

    @Transactional
    @Override
    public void delete(Long id) {
        this.productRepository.deleteById(id);
    }

    @Transactional
    @Override
    public ProducDTO getById(Long id) {
        return this.productRepository.findById(id).isPresent() ? this.productMapper.mapToDTO(this.productRepository.findById(id).get()) : null;
    }

}
