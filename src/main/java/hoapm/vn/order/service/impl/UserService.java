package hoapm.vn.order.service.impl;

import hoapm.vn.order.dto.UsersDTO;
import hoapm.vn.order.mapper.IUserMapper;
import hoapm.vn.order.repository.IUsersRepository;
import hoapm.vn.order.service.IUserService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class UserService implements IUserService {

    private final IUsersRepository usersRepository;

    private final IUserMapper userMapper;

    public UserService(IUsersRepository usersRepository, IUserMapper userMapper) {
        this.usersRepository = usersRepository;
        this.userMapper = userMapper;
    }

    @Override
    public List<UsersDTO> getAll() {
        return this.userMapper.mapToListDTO(this.usersRepository.findAll());
    }

    @Override
    public void upsert(UsersDTO user) {
        this.usersRepository.save(this.userMapper.mapToEntity(user));
    }

    @Override
    public void upsertBulk(List<UsersDTO> users) {
        this.usersRepository.saveAll(this.userMapper.mapToListEntity(users));
    }

    @Override
    public void delete(Long aLong) {

    }

    @Override
    public UsersDTO getById(Long id) {
        return this.usersRepository.findById(id).isPresent() ? this.userMapper.mapToDTO(this.usersRepository.findById(id).get()) : null;
    }
}
