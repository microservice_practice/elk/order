package hoapm.vn.order.service.impl;

import hoapm.vn.order.dto.OrdersDTO;
import hoapm.vn.order.entity.OrderEntity;
import hoapm.vn.order.entity.ProductEntity;
import hoapm.vn.order.entity.UserEntity;
import hoapm.vn.order.mapper.IOrderMapper;
import hoapm.vn.order.repository.IOrdersRepository;
import hoapm.vn.order.repository.IProductRepository;
import hoapm.vn.order.service.IOrderService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

@Service
public class OrderService implements IOrderService {

    private final IOrdersRepository ordersRepository;

    private final IOrderMapper orderMapper;

    private final IProductRepository productRepository;

    private final Logger logger = LoggerFactory.getLogger(OrderService.class.getName());

    public OrderService(IOrdersRepository ordersRepository, IOrderMapper orderMapper, IProductRepository productRepository) {
        this.ordersRepository = ordersRepository;
        this.orderMapper = orderMapper;
        this.productRepository = productRepository;
    }

    @Transactional
    @Override
    public List<OrdersDTO> getAll() {
        return this.orderMapper.mapToListDTO(this.ordersRepository.findAll());
    }

    @Override
    @Transactional
    public void upsert(OrdersDTO order) {
        OrderEntity entity = this.orderMapper.mapToEntity(order);
        if (!order.getProductIds().isEmpty()) {
            entity.setProducts(getProductEntitiesByIds(order.getProductIds()));
        }
        this.ordersRepository.save(entity);
    }

    @Override
    @Transactional
    public void upsertBulk(List<OrdersDTO> orders) {
        this.ordersRepository.saveAll(this.orderMapper.mapToListEntity(orders));
    }

    @Override
    @Transactional
    public void delete(Long id) {
        this.ordersRepository.deleteById(id);
    }

    @Override
    @Transactional
    public OrdersDTO getById(Long id) {
        OrderEntity order = this.ordersRepository.findById(id).isPresent() ? ordersRepository.findById(id).get() : null;
        if (!Objects.isNull(order)) {
            UserEntity user = order.getUsers();
            logger.info("User: {}", user);
        }
        return this.orderMapper.mapToDTO(order);
    }

    @Transactional
    public List<ProductEntity> getProductEntitiesByIds(List<Long> productIds) {
        List<ProductEntity> result;
        if (!productIds.isEmpty()) {
            result = this.productRepository.findAllById(productIds);
            return result;
        }
        return new ArrayList<>();
    }
}
