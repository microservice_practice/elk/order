package hoapm.vn.order.service;

import hoapm.vn.order.dto.UsersDTO;
import hoapm.vn.order.entity.UserEntity;

public interface IUserService extends IGenericService<UsersDTO,Long> {
}
