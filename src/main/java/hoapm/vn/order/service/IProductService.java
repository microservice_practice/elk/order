package hoapm.vn.order.service;

import hoapm.vn.order.dto.ProducDTO;
import hoapm.vn.order.entity.ProductEntity;

public interface IProductService extends IGenericService<ProducDTO, Long> {

}
