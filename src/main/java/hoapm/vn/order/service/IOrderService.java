package hoapm.vn.order.service;

import hoapm.vn.order.dto.OrdersDTO;
import hoapm.vn.order.entity.OrderEntity;

public interface IOrderService extends IGenericService<OrdersDTO, Long> {
}
