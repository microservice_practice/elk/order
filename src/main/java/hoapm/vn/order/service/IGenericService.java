package hoapm.vn.order.service;

import java.util.List;

public interface IGenericService<T, D> {
    List<T> getAll();

    void upsert(T t);

    void upsertBulk (List<T> ts);

    void delete(D d);

    T getById (Long id);

}
