package hoapm.vn.order.repository;

import hoapm.vn.order.entity.UserEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IUsersRepository extends JpaRepository<UserEntity,Long> {
}
