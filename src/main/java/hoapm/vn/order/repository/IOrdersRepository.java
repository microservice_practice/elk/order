package hoapm.vn.order.repository;

import hoapm.vn.order.entity.OrderEntity;
import org.springframework.data.jpa.repository.JpaRepository;

public interface IOrdersRepository extends JpaRepository<OrderEntity,Long> {
}
