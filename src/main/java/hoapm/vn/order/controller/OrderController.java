package hoapm.vn.order.controller;

import hoapm.vn.order.dto.OrdersDTO;
import hoapm.vn.order.service.IOrderService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("order")
public class OrderController {
    private final IOrderService orderService;

    public OrderController(IOrderService orderService) {
        this.orderService = orderService;
    }
    @PostMapping()
    public ResponseEntity<?> insert(@RequestBody OrdersDTO ordersDTO) {
        this.orderService.upsert(ordersDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }
    @GetMapping
    public ResponseEntity<?> getAll(){
        List<OrdersDTO> result = this.orderService.getAll();
        return new ResponseEntity<>(result,HttpStatus.OK);
    }
    @GetMapping("/{id}")
    public ResponseEntity<?> getById(@PathVariable Long id) {
        OrdersDTO result = this.orderService.getById(id);
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
    @PutMapping("/{id}")
    public ResponseEntity<?> update(@PathVariable Long id, @RequestBody OrdersDTO ordersDTO) {
        ordersDTO.setId(id);
        this.orderService.upsert(ordersDTO);
        return new ResponseEntity<>(HttpStatus.ACCEPTED);
    }
}
