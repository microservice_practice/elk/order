package hoapm.vn.order.controller;

import hoapm.vn.order.dto.ProducDTO;
import hoapm.vn.order.dto.UsersDTO;
import hoapm.vn.order.mapper.IUserMapper;
import hoapm.vn.order.service.IUserService;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@RestController
@RequestMapping("/user")
public class UserController {
    private final IUserService userService;

    public UserController(IUserService userService) {
        this.userService = userService;
    }
    @PostMapping()
    public ResponseEntity<?> insert(@RequestBody UsersDTO usersDTO) {
        this.userService.upsert(usersDTO);
        return new ResponseEntity<>(HttpStatus.CREATED);
    }

    @GetMapping()
    public ResponseEntity<?> getAll() {
        List<UsersDTO> result = this.userService.getAll();
        return new ResponseEntity<>(result, HttpStatus.OK);
    }
}
